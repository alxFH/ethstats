package ethcorp.ethstats10.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ethcorp.ethstats10.Model.GeneralModel;
import ethcorp.ethstats10.Model.WorkerModel;
import ethcorp.ethstats10.R;

public class ModelRecyclerViewAdapter extends RecyclerView.Adapter<ModelRecyclerViewAdapter.ViewHolder> {

    private List<WorkerModel> mModelList = new ArrayList<>();

    public ModelRecyclerViewAdapter(List<WorkerModel> overviewList) {
        mModelList = overviewList;
    }


    @Override
    public ModelRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_overview, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        WorkerModel currentModel = mModelList.get(position);

        holder.mTitle.setText("Minername: "+ currentModel.getWorker());
        holder.mDetail.setText("current Hashrate: "+ currentModel.getHashrate());
    }

    @Override
    public int getItemCount() {
        return mModelList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView  mTitle;
        protected TextView  mDetail;

        public ViewHolder(View v) {
            super(v);

            mTitle  = (TextView)  v.findViewById(R.id.item_title);
            mDetail = (TextView)  v.findViewById(R.id.item_detail);
        }
    }
}