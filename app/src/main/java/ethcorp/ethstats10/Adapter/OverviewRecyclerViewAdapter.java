package ethcorp.ethstats10.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ethcorp.ethstats10.Model.OverviewModel;
import ethcorp.ethstats10.R;


public class OverviewRecyclerViewAdapter extends RecyclerView.Adapter<OverviewRecyclerViewAdapter.ViewHolder> {

    private List<String> mOverviewList = new ArrayList<>();

    public OverviewRecyclerViewAdapter(List<String> overviewList) {
        mOverviewList = overviewList;
    }


    @Override
    public OverviewRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_overview, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String[] data = mOverviewList.get(position).split("#");

        holder.mTitle.setText(data[0]);
        holder.mDetail.setText(data[1]);
    }

    @Override
    public int getItemCount() {
        return mOverviewList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView  mTitle;
        protected TextView  mDetail;

        public ViewHolder(View v) {
            super(v);

            mTitle  = (TextView)  v.findViewById(R.id.item_title);
            mDetail = (TextView)  v.findViewById(R.id.item_detail);
        }
    }
}