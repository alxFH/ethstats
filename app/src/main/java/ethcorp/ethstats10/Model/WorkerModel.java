package ethcorp.ethstats10.Model;

public class WorkerModel {

    private String mWorker;
    private String mHashrate;
    private String mRepHashrate;


    public String getWorker() {
        return mWorker;
    }

    public void setWorker(String mWorker) {
        this.mWorker = mWorker;
    }

    public String getHashrate() {
        return mHashrate;
    }

    public void setHashrate(String mHashrate) {
        this.mHashrate = mHashrate;
    }

    public String getRepHashrate() {
        return mRepHashrate;
    }

    public void setRepHashrate(String mRepHashrate) {
        this.mRepHashrate = mRepHashrate;
    }

}
