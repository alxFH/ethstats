package ethcorp.ethstats10.Model;

public class PayoutModel {


    private String mMiner;
    private String mAmount;
    private String mPaidOn;
    private String mTxHash;


    public String getMiner() {
        return mMiner;
    }

    public void setMiner(String mMiner) {
        this.mMiner = mMiner;
    }

    public String getAmount() {
        return mAmount;
    }

    public void setAmount(String mAmount) {
        this.mAmount = mAmount;
    }

    public String getPaidOn() {
        return mPaidOn;
    }

    public void setPaidOn(String mPaidOn) {
        this.mPaidOn = mPaidOn;
    }
    public String getTxHash() {
        return mTxHash;
    }

    public void setTxHash(String mTxHash) {
        this.mTxHash = mPaidOn;
    }
}
