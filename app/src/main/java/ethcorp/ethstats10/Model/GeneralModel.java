package ethcorp.ethstats10.Model;

public class GeneralModel {

    private String mAddress;
    private String mHashRateRate;
    private String mRephashRate;
    private String mAvghashRate;
    private String mEthMin;
    private String mUsdMin;
    private String mBtcMin;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getHashRate() {
        return mHashRateRate;
    }

    public void setHashRate(String mHashRateRate) {
        this.mHashRateRate = mHashRateRate;
    }

    public String getRepHashRate() {
        return mRephashRate;
    }

    public void setRepHashRate(String mRephashRate) {
        this.mRephashRate = mRephashRate;
    }

    public String getAvgHashRate() {
        return mAvghashRate;
    }

    public void setAvgHashRate(String mAvghashRate) {
        this.mAvghashRate = mAvghashRate;
    }

    public String getEthMin() {
        return mEthMin;
    }

    public void setEthMin(String mEthMin) {
        this.mEthMin = mEthMin;
    }

    public String getUsdMin() {
        return mUsdMin;
    }

    public void setUsdMin(String mUsdMin) {
        this.mUsdMin = mUsdMin;
    }

    public String getBtcMin() {
        return mBtcMin;
    }

    public void setBtcMin(String mBtcMin) {
        this.mBtcMin = mBtcMin;
    }
}
