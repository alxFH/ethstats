package ethcorp.ethstats10.Model;

import java.util.ArrayList;
import java.util.List;


public class OverviewModel {

    private List<WorkerModel> mWorkerList = new ArrayList<>();
    private List<PayoutModel> mPayoutList = new ArrayList<>();
    private List<RoundModel> mRoundList = new ArrayList<>();
    private GeneralModel mGeneralModel = new GeneralModel();


    public List<PayoutModel> getPayoutList() {
        return mPayoutList;
    }

    public void setPayoutList(List<PayoutModel> mPayoutList) {
        this.mPayoutList = mPayoutList;
    }

    public List<RoundModel> getRoundList() {
        return mRoundList;
    }

    public void setRoundList(List<RoundModel> mRoundList) {
        this.mRoundList = mRoundList;
    }

    public List<WorkerModel> getWorkerList() {
        return mWorkerList;
    }

    public void setWorkerList(List<WorkerModel> mWorkerList) {
        this.mWorkerList = mWorkerList;
    }

    public GeneralModel getGeneralModel() {
        return mGeneralModel;
    }

    public void setGeneralModel(GeneralModel mGeneralModel) {
        this.mGeneralModel = mGeneralModel;
    }

}
