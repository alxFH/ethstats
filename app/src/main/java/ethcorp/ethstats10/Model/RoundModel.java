package ethcorp.ethstats10.Model;

public class RoundModel {
    private String mId;
    private String mHash;

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getHash() {
        return mHash;
    }

    public void setHash(String mHash) {
        this.mHash = mHash;
    }

}
