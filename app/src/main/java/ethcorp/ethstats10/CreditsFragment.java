package ethcorp.ethstats10;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.RelativeLayout;

import com.vansuita.materialabout.builder.AboutBuilder;
import com.vansuita.materialabout.views.AboutView;

public class CreditsFragment extends Fragment {

    private View mLayout;

    public CreditsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //gibt erzeugte View zurück
        mLayout = inflater.inflate(R.layout.fragment_credits, container, false);

        return mLayout;
    }

    @Override
    public void onStart() {
        super.onStart();

        AboutView view = AboutBuilder.with(getContext())
                .setPhoto(R.mipmap.profile_picture)
                .setCover(R.mipmap.profile_cover)
                .setName("Alexander Härdrich")
                .setSubTitle("Mobile Developer")
                .setBrief("Created from miner for miners.")
                .setAppIcon(R.mipmap.ic_launcher)
                .setAppName(R.string.app_name)
                .addGooglePlayStoreLink("8002078663318221363")
                .addGitHubLink("user")
                .addFacebookLink("user")
                .addFiveStarsAction()
                .setVersionNameAsAppSubTitle()
                .addShareAction(R.string.app_name)
                .setLinksAnimated(true)
                .setShowAsCard(true)
                .build();

        ((RelativeLayout)mLayout).addView(view);
    }
}
