package ethcorp.ethstats10;


import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ethcorp.ethstats10.Controller.GlobalPreferences;
import ethcorp.ethstats10.Importer.DataImport;
import ethcorp.ethstats10.Model.OverviewModel;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GraphsFragment extends Fragment {

    private View mLayout;
    private SwipeRefreshLayout mPullToRefresh;
    private OverviewModel mOverviewModel;

    private LineChart mChart;

    public GraphsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //gibt erzeugte View zurück
        mLayout = inflater.inflate(R.layout.fragment_graphs, container, false);
        return mLayout;
    }

    @Override
    public void onStart() {
        super.onStart();
        makeStatsRequest();
    }

    private void updateView(){

        mPullToRefresh = (SwipeRefreshLayout) mLayout.findViewById(R.id.swipeRefresh);
        mPullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                makeStatsRequest();
                mPullToRefresh.setRefreshing(true);
            }
        });
    }

    private void drawGraphLine(){

        ArrayList<Entry> Values = new ArrayList<>();

        for(int i = 0; i < mOverviewModel.getRoundList().size(); i++){
            System.out.println("round: " + mOverviewModel.getRoundList().get(i).getId() + " hash: " + mOverviewModel.getRoundList().get(i).getHash());

            Values.add(new Entry( Float.parseFloat(mOverviewModel.getRoundList().get(i).getId()), Float.parseFloat(mOverviewModel.getRoundList().get(i).getHash())));
        }

        mChart = (LineChart) mLayout.findViewById(R.id.chart1);
        LineDataSet set1 = new LineDataSet(Values, "Data Set 1");
        set1.setFillAlpha(110);

        System.out.println(mChart);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        LineData data = new LineData(dataSets);

        mChart.setData(data);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLayout.findViewById(R.id.chart1).performClick();

                long downTime = SystemClock.uptimeMillis();
                long eventTime = SystemClock.uptimeMillis() + 100;
                float x = 0.0f;
                float y = 0.0f;
                // List of meta states found here:     developer.android.com/reference/android/view/KeyEvent.html#getMetaState()
                int metaState = 0;
                MotionEvent motionEvent = MotionEvent.obtain(
                        downTime,
                        eventTime,
                        MotionEvent.ACTION_UP,
                        x,
                        y,
                        metaState
                );

                // Dispatch touch event to view
                mLayout.findViewById(R.id.chart1).dispatchTouchEvent(motionEvent);
            }
        }, 1000);

    }

    private void makeStatsRequest(){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://ethermine.org/api/miner_new/"+ GlobalPreferences.getInstance().getLoginAdress())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateView();
                            mPullToRefresh.setRefreshing(false);
                        }
                    });
                    throw new IOException("Unexpected code " + response.code());
                }
                String responseString = response.body().string();
                response.body().close();

                mOverviewModel = DataImport.fillData(responseString, false );

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateView();
                        drawGraphLine();
                        mPullToRefresh.setRefreshing(false);
                    }
                });

            }
        });
    }
}
