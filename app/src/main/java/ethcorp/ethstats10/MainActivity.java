package ethcorp.ethstats10;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import ethcorp.ethstats10.Controller.GlobalPreferences;
import ethcorp.ethstats10.Controller.ViewController;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    NotificationCompat.Builder notification;
    private static final int unqueID = 45612;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);
    }

    public void testMyNotification(){
        notification.setSmallIcon(R.drawable.ic_menu_manage);
        notification.setTicker("This is the Ticker");
        notification.setContentTitle("Head Title of Noti");
        notification.setContentText("Miner is broken!");

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(unqueID,notification.build());
    }

    @Override
    protected void onStart() {
        super.onStart();

        //support für alte API
        ViewController.getInstance().init(this);
        if (GlobalPreferences.getInstance().getLoginAdress().equals("0")){
            ViewController.getInstance().changeFragment(new LoginFragment());
        } else {
            ViewController.getInstance().changeFragment(new StartFragment());
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {

            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_start) {
            ViewController.getInstance().changeAddFragment(new StartFragment());
        } else if (id == R.id.nav_miners) {
            ViewController.getInstance().changeAddFragment(new MinersFragment());
        } else if (id == R.id.nav_graphs) {
            //testMyNotification();
            ViewController.getInstance().changeAddFragment(new GraphsFragment());
        } else if (id == R.id.nav_manage) {
            ViewController.getInstance().changeAddFragment(new SettingsFragment());
        } else if (id == R.id.nav_logout) {

            SharedPreferences sharedPrefs = getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.clear();
            editor.commit();

            ViewController.getInstance().changeAddFragment(new LoginFragment());
        } else if (id == R.id.nav_share) {
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, "Share this APP!", duration);
            toast.show();
        } else if (id == R.id.nav_credits) {
            ViewController.getInstance().changeAddFragment(new CreditsFragment());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
