package ethcorp.ethstats10;


import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import ethcorp.ethstats10.Service.WarningService;


public class SettingsFragment extends Fragment {

    private View mLayout;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //gibt erzeugte View zurück
        mLayout = inflater.inflate(R.layout.fragment_settings, container, false);
        return mLayout;
    }

    @Override
    public void onStart() {
        super.onStart();

        Switch s = (Switch) mLayout.findViewById(R.id.switch1);
        s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){

                    getActivity().startService(new Intent(getActivity(),WarningService.class));

                } else {

                    getActivity().stopService(new Intent(getActivity(),WarningService.class));
                }

            }
        });
        //set check flag if active
        if(WarningService.checkIsActive){
            s.setChecked(true);
        } else {
            s.setChecked(false);
        }
    }
}
