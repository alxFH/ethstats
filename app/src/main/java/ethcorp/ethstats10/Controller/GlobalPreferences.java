package ethcorp.ethstats10.Controller;

import android.content.Context;
import android.content.SharedPreferences;

import ethcorp.ethstats10.MainActivity;
import ethcorp.ethstats10.R;

public class GlobalPreferences {
    private static final GlobalPreferences ourInstance = new GlobalPreferences();
    private MainActivity mActivity;

    private static String SHARED_PREFS = "ethminer";

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    private static String LOGIN     = "loginAddress";
    private static String RHASH     = "repHash";
    private static String RHASHL     = "repHashL";
    private static String TEMP_DATA = "cacheTemp";


    public static GlobalPreferences getInstance() {
        return ourInstance;
    }

    private GlobalPreferences() {
        mActivity = ViewController.getInstance().getActivity();
        mSharedPreferences = mActivity.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public void setLoginAdress(String address) {
        mEditor.putString(LOGIN, address).commit();
    }

    public void setRepHash(String rHash) {
        mEditor.putString(RHASH, rHash).commit();
    }

    public void setRepHashLast(String rHashL) {
        mEditor.putString(RHASHL, rHashL).commit();
    }

    public String getLoginAdress(){
        return mSharedPreferences.getString(LOGIN,"0");
    }

    public String getRepHash(){
        return mSharedPreferences.getString(RHASH,"0");
    }

    public String getRepHashLast(){
        return mSharedPreferences.getString(RHASHL,"0");
    }

    public void setTempData(String json) {
        mEditor.putString(TEMP_DATA, json).commit();
    }

    public String getTempData(){
        return mSharedPreferences.getString(TEMP_DATA,"0");
    }


}
