package ethcorp.ethstats10.Controller;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import ethcorp.ethstats10.MainActivity;
import ethcorp.ethstats10.R;

public class ViewController {

    private MainActivity mActivity;
    private static ViewController ourInstance = new ViewController();

    public static ViewController getInstance() {
        return ourInstance;
    }
    private ViewController() {
    }

    public void init(MainActivity activity){
        mActivity = activity;
    }

    public void changeFragment(Fragment fragment){
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainView, fragment)
                .commit();
    }

    public void changeAddFragment(Fragment fragment){
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.mainView, fragment)
                .commit();
    }

    public MainActivity getActivity(){
        return mActivity;
    }
}
