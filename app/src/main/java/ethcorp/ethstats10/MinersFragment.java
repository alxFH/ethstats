package ethcorp.ethstats10;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ethcorp.ethstats10.Adapter.ModelRecyclerViewAdapter;
import ethcorp.ethstats10.Adapter.OverviewRecyclerViewAdapter;
import ethcorp.ethstats10.Controller.GlobalPreferences;
import ethcorp.ethstats10.Controller.ViewController;
import ethcorp.ethstats10.Importer.DataImport;
import ethcorp.ethstats10.Model.OverviewModel;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MinersFragment extends Fragment {

    private View mLayout;
    private RelativeLayout mLoadlingLayout;
    private SwipeRefreshLayout mPullToRefresh;
    private OverviewModel mOverviewModel;

    public MinersFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mLayout = inflater.inflate(R.layout.fragment_miners, container, false);
        mLoadlingLayout = (RelativeLayout) mLayout.findViewById(R.id.loadingLayoutMiners);

        return mLayout;
    }

    @Override
    public void onStart() {
        super.onStart();
        makeStatsRequest();
    }

    private void updateView(){

        RecyclerView recyclerView = (RecyclerView) mLayout.findViewById(R.id.overviewRecyclerViewMiners);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        ModelRecyclerViewAdapter adapter = new ModelRecyclerViewAdapter(mOverviewModel.getWorkerList());
        recyclerView.setAdapter(adapter);

        mPullToRefresh = (SwipeRefreshLayout) mLayout.findViewById(R.id.swipeRefresh);
        mPullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                makeStatsRequest();
                mPullToRefresh.setRefreshing(true);
            }
        });


        mLoadlingLayout.setVisibility(View.GONE);
    }

    private void makeStatsRequest(){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://ethermine.org/api/miner_new/"+ GlobalPreferences.getInstance().getLoginAdress())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateView();
                            mPullToRefresh.setRefreshing(false);
                        }
                    });
                    throw new IOException("Unexpected code " + response.code());
                }
                String responseString = response.body().string();
                response.body().close();

                mOverviewModel = DataImport.fillData(responseString, false);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateView();
                        mPullToRefresh.setRefreshing(false);
                    }
                });

            }
        });
    }
}
