package ethcorp.ethstats10;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ethcorp.ethstats10.Adapter.OverviewRecyclerViewAdapter;
import ethcorp.ethstats10.Controller.GlobalPreferences;
import ethcorp.ethstats10.Controller.ViewController;
import ethcorp.ethstats10.Importer.DataImport;
import ethcorp.ethstats10.Model.OverviewModel;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class LoginFragment extends Fragment {

    private View mLayout;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //gibt erzeugte View zurück
        mLayout = inflater.inflate(R.layout.fragment_login, container, false);
        return mLayout;
    }

    @Override
    public void onStart() {
        super.onStart();

        Button login = (Button) mLayout.findViewById(R.id.loginButton);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

        final EditText myAddress = (EditText) mLayout.findViewById(R.id.edit_message);

        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url("https://ethermine.org/api/miner_new/"+myAddress.getText())
                .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        throw new IOException("Unexpected code " + response.code());
                    }

                    if (response.isSuccessful()){
                        String responseString = response.body().string();
                        response.body().close();

                        GlobalPreferences.getInstance().setLoginAdress(String.valueOf(myAddress.getText()));

                        DataImport.fillData(responseString,false);

                        ViewController.getInstance().changeFragment(new StartFragment());
                    }
                }
            });
            }
        });

    }
}
