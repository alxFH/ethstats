package ethcorp.ethstats10.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.RelativeLayout;

import java.io.IOException;

import ethcorp.ethstats10.Controller.GlobalPreferences;
import ethcorp.ethstats10.Controller.ViewController;
import ethcorp.ethstats10.Importer.DataImport;
import ethcorp.ethstats10.MainActivity;
import ethcorp.ethstats10.Model.OverviewModel;
import ethcorp.ethstats10.R;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Alex on 16.01.2018.
 */

public class WarningService extends Service {

    private Handler mHanlder;
    private Runnable mRunnable;
    private View mLayout;
    private RelativeLayout mLoadlingLayout;
    private SwipeRefreshLayout mPullToRefresh;
    private OverviewModel mOverviewModel;
    private MainActivity mActivity;


    public static boolean checkIsActive;

    private NotificationCompat.Builder n;
    private static final int uniqueID = 45612;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mActivity = ViewController.getInstance().getActivity();
        n = new NotificationCompat.Builder(mActivity);
        n.setAutoCancel(true);

        checkIsActive = true;
        if (mHanlder == null) {
            mHanlder = new Handler();
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    // req
                    makeStatsRequest();
                    if((Float.parseFloat((GlobalPreferences.getInstance().getRepHash()).replace(" ","")) - Float.parseFloat((GlobalPreferences.getInstance().getRepHashLast()).replace(" ",""))) > 0 ){
                        System.out.println("Miner lost some Performance!");

                        //build notification
                        n.setSmallIcon(R.mipmap.ic_launcher_round);
                        n.setBadgeIconType(R.mipmap.ic_launcher_round);
                        n.setTicker("Miner-Notification");
                        n.setWhen(System.currentTimeMillis());
                        n.setContentTitle("Miner-Notification!");
                        n.setContentText("Your Miner-Performance dropped!");

                        Intent intent = new Intent(mActivity, MainActivity.class);
                        PendingIntent pendingIntent = PendingIntent.getActivity(mActivity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                        n.setContentIntent(pendingIntent);

                        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        nm.notify(uniqueID, n.build());

                    } else {
                        System.out.println("Miner increase Power/Performance!");
                    }
                    mHanlder.postDelayed(this, 3000);
                }
            };
            mHanlder.postDelayed(mRunnable, 3000);
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        mHanlder.removeCallbacks(mRunnable);
        checkIsActive = false;
        System.out.println("Service stopped!");
        super.onDestroy();
    }

    private void makeStatsRequest(){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://ethermine.org/api/miner_new/"+ GlobalPreferences.getInstance().getLoginAdress())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response.code());
                }
                String responseString = response.body().string();
                response.body().close();

                mOverviewModel = DataImport.fillData(responseString,true);
            }
        });
    }




}
