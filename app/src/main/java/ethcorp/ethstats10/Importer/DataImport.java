package ethcorp.ethstats10.Importer;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ethcorp.ethstats10.Controller.GlobalPreferences;
import ethcorp.ethstats10.Model.GeneralModel;
import ethcorp.ethstats10.Model.OverviewModel;
import ethcorp.ethstats10.Model.PayoutModel;
import ethcorp.ethstats10.Model.WorkerModel;
import ethcorp.ethstats10.Model.RoundModel;

public class DataImport {
    public static OverviewModel fillData(String response, Boolean service){
        OverviewModel overviewModel = new OverviewModel();
        try {
            //Generate JSONObject
            JSONObject data = new JSONObject(response);

            //generate ArrayLists
            List<GeneralModel> generalModelList = new ArrayList<>();
            List<PayoutModel> payoutModelList = new ArrayList<>();
            List<RoundModel> roundModelList = new ArrayList<>();
            List<WorkerModel> workerModelList = new ArrayList<>();

            //Fill GeneralList
            //String address = data.optString("address", "0");
            GeneralModel modelG = new GeneralModel();
            modelG.setAddress(data.optString("address", "0"));
            modelG.setHashRate(data.optString("hashRate", "0"));
            modelG.setAvgHashRate(data.optString("avgHashrate", "0"));
            modelG.setRepHashRate(data.optString("reportedHashRate", "0"));
            modelG.setEthMin(data.optString("ethPerMin", "0"));
            modelG.setBtcMin(data.optString("btcPerMin", "0"));
            modelG.setUsdMin(data.optString("usdPerMin", "0"));

            //set cache data
            if(GlobalPreferences.getInstance().getRepHash().equals("0")){
                GlobalPreferences.getInstance().setRepHash((data.optString("reportedHashRate", "0")).replace("MH/s",""));
            } else {
                GlobalPreferences.getInstance().setRepHashLast((data.optString("reportedHashRate", "0")).replace("MH/s",""));
            }

            generalModelList.add(modelG);
            overviewModel.setGeneralModel(modelG);

            //Fill PayoutList
            JSONArray dataPayouts = data.optJSONArray("payouts");
            for(int i = 0; i < dataPayouts.length(); i++){
                // new payout model
                JSONObject currentPayout = (JSONObject) dataPayouts.get(i);
                PayoutModel modelP = new PayoutModel();

                modelP.setAmount(currentPayout.optString("amount", "0"));
                modelP.setMiner(currentPayout.optString("miner", "0"));
                modelP.setPaidOn(currentPayout.optString("start", "0"));
                modelP.setTxHash(currentPayout.optString("txHash", "0"));

                payoutModelList.add(modelP);
            }
            overviewModel.setPayoutList(payoutModelList);

            JSONArray dataRounds = data.optJSONArray("rounds");
            //int x = 10;
            for(int i = 0; i < dataRounds.length(); i++){
                // new payout model
                JSONObject currentRound = (JSONObject) dataRounds.get(i);
                RoundModel modelR = new RoundModel();

                String hash = currentRound.optString("work", "0");
                Float hash1 = (Float.parseFloat(hash))/1000000;

                modelR.setId(Integer.toString(i));
                modelR.setHash(hash1.toString());

                roundModelList.add(modelR);
            }
            overviewModel.setRoundList(roundModelList);

            //Fill WorkerList
            JSONObject dataWorkers = data.optJSONObject("workers");

            List<String> workerNames = new ArrayList<>();
            Iterator workerKeys = dataWorkers.keys();
            while(workerKeys.hasNext()){
                workerNames.add(workerKeys.next().toString());
            }

            for(int i = 0; i < workerNames.size(); i++){
                // new payout model
                JSONObject currentWorker = dataWorkers.getJSONObject(workerNames.get(i));
                WorkerModel modelW = new WorkerModel();

                modelW.setWorker(currentWorker.optString("worker", "0"));
                modelW.setHashrate(currentWorker.optString("hashrate", "0"));
                modelW.setRepHashrate(currentWorker.optString("reportedHashRate", "0"));

                workerModelList.add(modelW);
            }

            overviewModel.setWorkerList(workerModelList);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        GlobalPreferences.getInstance().setTempData(response);
        return overviewModel;
    }
}
